# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master
LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use to work with various clouds"

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-euxfo", "pipefail", "-c"]

ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

# hadolint ignore=DL3017,DL3018,SC2046
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# We also bind-mount goss and trivy binaries from the docker-builder image
	--mount=type=bind,source=goss,target=/bin/goss,readonly \
	--mount=type=bind,source=trivy,target=/bin/trivy,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# Main flow
	apk -qq --no-cache upgrade \
	# install gcloud: TODO: shasums, and figure out kubectl on arm64
	&& apk -qq --no-cache add \
		py3-crcmod \
		python3 \
	&& mkdir -p "/usr/local/gcloud" \
	&& wget -qqO - "https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz" \
		| tar -C "/usr/local/gcloud" -zxf - \
	&& /usr/local/gcloud/google-cloud-sdk/install.sh --quiet --override-components alpha beta core gsutil kubectl gke-gcloud-auth-plugin \
	&& gcloud config set component_manager/disable_update_check true \
	&& gcloud config set core/disable_prompts true \
	&& gcloud config set core/disable_usage_reporting true \
	&& gcloud config set core/show_structured_logs always \
	&& gcloud config set survey/disable_prompts true \
	# remove all the kubectl versions except latest
	&& find /usr/local/gcloud/google-cloud-sdk/bin/ -name 'kubectl\.*' -delete \
	# if on arm64, add libc6-compat, required for kubectl
	&& if [[ "arm64" == "${TARGETARCH}" ]]; then \
		apk -qq --no-cache add libc6-compat gcompat ; \
	fi \
	# cleanup logs and backups
	&& rm -rf "${HOME}/.config/gcloud/logs" \
	&& rm -rf "/usr/local/gcloud/google-cloud-sdk/.install/.backup/" \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"
