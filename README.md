# cloud-toolbelt

Image to work with clouds.

This image is multiarch, see os/architectures we build in .gitlab-ci.yml

As of now:
 - gcloud
 - kubectl
